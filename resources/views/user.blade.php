<!DOCTYPE html>
<html>
<head>
	<title>Form</title>
	<link rel="stylesheet" href="{{asset("css/app.css")}}"></link>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
</head>
<body>
	<div class="row">
		<div class="alert alert-danger col-md-12" id="errors" style="display: none;">
			
		</div>
	</div>
	<div class="form-wrapper">
		<form id="user" method="POST" action="{{action("UserController@create")}}">
			{{csrf_field()}}
			<div class="form-block">
				<div class="step-header header-gradient" data-step="1">
					Step 1: Your details
				</div>
				<div class="step-body" data-step-body="1">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group margin-small ">
								<label class="no-margin">First name</label>
								<input class="form-control small" type="text" value="Tudose" name="first-name" required>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group margin-small ">
								<label class="no-margin" >Surname</label>
								<input class="form-control small" type="text" value="Valentin" name="surname" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group margin-small">
								<label class="no-margin" >Email adress</label>
								<input class="form-control small" type="email" value="valentin.tudose@gmail.com" name="email" value="" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2 offset-md-10">
							<button class="btn custom-button" id="next-1"> Next > </button>
						</div>
					</div>
				</div>
				<div class="step-header header-gradient" data-step="2">
					Step 2: Comments
				</div>
				<div class="step-body" data-step-body="2" style="display: none;">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group margin-small ">
								<label class="no-margin">Telephone number</label>
								<input class="form-control small" type="text" value="+4074467219" name="phone-number" required>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group margin-small ">
								<label class="no-margin" >Gender</label>
								<select class="form-control small" name="gender" required>
									<option value="1" selected>Masculin</option>
									<option value="2">Feminin</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group margin-small">
								<label class="no-margin" >Birth date</label>
								<input class="form-control small" type="text" value="12/21/1994" name="birth-date" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2 offset-md-10">
							<button class="btn custom-button" id="next-2"> Next > </button>
						</div>
					</div>
				</div>
				<div class="step-header header-gradient" data-step="3">
					Step 2: More comments
				</div>
				<div class="step-body" style="display: none;" data-step-body="3">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group margin-small ">
								<label class="no-margin">Comments: </label>
								<textarea name="comments" class="form-control"></textarea>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2 offset-md-10">
							<button class="btn custom-button" type="submit"> Save </button>
						</div>
					</div>
				</div>
			</div>
		</form>
    </div>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="{{asset("js/app.js")}}"></script>
</body>
</html>