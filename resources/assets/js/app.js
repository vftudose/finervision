function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validatePhoneNumber(number) {
	var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
	return re.test(number);
}

function validateStepOne() {

	let firstNameContainer = $('input[name="first-name"]');
	let surnameContainer   = $('input[name="surname"]');
	let emailContainer     = $('input[name="email"]'); 

	let firstName = firstNameContainer.val();
	let surname   = surnameContainer.val();
	let email     = emailContainer.val();

	if(firstName.length == 0) {
		return {
			"pass":false,
			"message":"First name can't be empty!",
			"input":firstNameContainer,
			"clearText":false
		};
	}

	if(surname.length == 0) {
		return {
			"pass":false,
			"message":"Surname can't be empty!",
			"input":surnameContainer,
			"clearText":false
		};
	}

	if(email.length == 0) {
		return {
			"pass":false,
			"message":"Email can't be empty!",
			"input":emailContainer,
			"clearText":false
		};
	}

	if(! validateEmail(email)){
		return {
			"pass":false,
			"message":"Email invalid!",
			"input":emailContainer,
			"clearText":true
		};
	}

	return {
		"pass":true
	}
}

function validateStepTwo(){
	let phoneNumberContainer 	= $('input[name="phone-number"]');

	let phoneNumber = phoneNumberContainer.val();
	
	if(phoneNumber.length == 0){
		return {
			"pass":false,
			"message":"Phone number can't be empty!",
			"input":phoneNumberContainer,
			"clearText":false
		};
	}

	if(! validatePhoneNumber(phoneNumber)) {
		return {
			"pass":false,
			"message":"Phone number invalid!",
			"input":phoneNumberContainer,
			"clearText":true
		};
	}

	return {
		"pass":true
	}
}

function onSuccess(response){
	if(response.status == 200) {

	}
}

function onError(response) {

	

	if(response.status == 422) {
		
		$.each(response.responseJSON.errors,function(index,value){
			appendError(value);
		});
	}
}

function appendError(message) {
	let errorContainer = $('#errors');
	
	errorContainer.empty();
	errorContainer.show();
	errorContainer.append(message);
}

$(document).ready(function(){
	$('#next-1').on('click',function(event){
		event.preventDefault();
		let validate = validateStepOne();

		if(! validate.pass) {

			validate.input.focus();
			validate.input.addClass('is-invalid');
			validate.input.attr("placeholder",validate.message);

			if(validate.clearText) {
				validate.input.val("");
			}
		}

		if(validate.pass) {
			$('div[data-step-body="1"]').slideUp();
			$('div[data-step-body="2"]').slideDown();
		}
	});

	$('#next-2').on('click',function(event){
		event.preventDefault();
		let validate = validateStepTwo();

		if(! validate.pass) {
			validate.input.focus();
			validate.input.addClass('is-invalid');
			validate.input.attr("placeholder",validate.message);

			if(validate.clearText) {
				validate.input.val("");
			}
		}

		if(validate.pass) {
			$('div[data-step-body="2"]').slideUp();
			$('div[data-step-body="3"]').slideDown();
		}	
	});

	$('#user').on('submit',function(event) {
		event.preventDefault();

		let data 	= $(this).serialize();
		let action 	= $(this).attr("action");
		
		$.ajax({
			url:action,
			method:"POST",
			data:data,
			success:function(response){
				onSuccess(response);
			},
			error:function(response){
				onError(response);
			}

		})
	});


	$('input').on('keyup',function(){
		$(this).removeClass('is-invalid');
		$(this).attr('placeholder',"");
	});

	$('input[name="birth-date"]').datepicker();
});