/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(3);


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__) {

"use strict";
function validateEmail(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}

function validatePhoneNumber(number) {
	var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
	return re.test(number);
}

function validateStepOne() {

	var firstNameContainer = $('input[name="first-name"]');
	var surnameContainer = $('input[name="surname"]');
	var emailContainer = $('input[name="email"]');

	var firstName = firstNameContainer.val();
	var surname = surnameContainer.val();
	var email = emailContainer.val();

	if (firstName.length == 0) {
		return {
			"pass": false,
			"message": "First name can't be empty!",
			"input": firstNameContainer,
			"clearText": false
		};
	}

	if (surname.length == 0) {
		return {
			"pass": false,
			"message": "Surname can't be empty!",
			"input": surnameContainer,
			"clearText": false
		};
	}

	if (email.length == 0) {
		return {
			"pass": false,
			"message": "Email can't be empty!",
			"input": emailContainer,
			"clearText": false
		};
	}

	if (!validateEmail(email)) {
		return {
			"pass": false,
			"message": "Email invalid!",
			"input": emailContainer,
			"clearText": true
		};
	}

	return {
		"pass": true
	};
}

function validateStepTwo() {
	var phoneNumberContainer = $('input[name="phone-number"]');

	var phoneNumber = phoneNumberContainer.val();

	if (phoneNumber.length == 0) {
		return {
			"pass": false,
			"message": "Phone number can't be empty!",
			"input": phoneNumberContainer,
			"clearText": false
		};
	}

	if (!validatePhoneNumber(phoneNumber)) {
		return {
			"pass": false,
			"message": "Phone number invalid!",
			"input": phoneNumberContainer,
			"clearText": true
		};
	}

	return {
		"pass": true
	};
}

function onSuccess(response) {
	if (response.status == 200) {}
}

function onError(response) {

	if (response.status == 422) {

		$.each(response.responseJSON.errors, function (index, value) {
			appendError(value);
		});
	}
}

function appendError(message) {
	var errorContainer = $('#errors');

	errorContainer.empty();
	errorContainer.show();
	errorContainer.append(message);
}

$(document).ready(function () {
	$('#next-1').on('click', function (event) {
		event.preventDefault();
		var validate = validateStepOne();

		if (!validate.pass) {

			validate.input.focus();
			validate.input.addClass('is-invalid');
			validate.input.attr("placeholder", validate.message);

			if (validate.clearText) {
				validate.input.val("");
			}
		}

		if (validate.pass) {
			$('div[data-step-body="1"]').slideUp();
			$('div[data-step-body="2"]').slideDown();
		}
	});

	$('#next-2').on('click', function (event) {
		event.preventDefault();
		var validate = validateStepTwo();

		if (!validate.pass) {
			validate.input.focus();
			validate.input.addClass('is-invalid');
			validate.input.attr("placeholder", validate.message);

			if (validate.clearText) {
				validate.input.val("");
			}
		}

		if (validate.pass) {
			$('div[data-step-body="2"]').slideUp();
			$('div[data-step-body="3"]').slideDown();
		}
	});

	$('#user').on('submit', function (event) {
		event.preventDefault();

		var data = $(this).serialize();
		var action = $(this).attr("action");

		$.ajax({
			url: action,
			method: "POST",
			data: data,
			success: function success(response) {
				onSuccess(response);
			},
			error: function error(response) {
				onError(response);
			}

		});
	});

	$('input').on('keyup', function () {
		$(this).removeClass('is-invalid');
		$(this).attr('placeholder', "");
	});

	$('input[name="birth-date"]').datepicker();
});

/***/ }),
/* 2 */,
/* 3 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);