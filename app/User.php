<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Carbon\Carbon;
class User extends Authenticatable
{
    use Notifiable;

    const GENDER_MALE      = 1;
    const GENDER_FEMALE    = 2;
    /*
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "first_name","last_name","phone_number","email","password","gender","comments","birth_date" 
    ];

    public $timestamps = false;

    public function getValidationRules() {
        return [
            "first-name"    => "required|max:255",
            "surname"       => "required|max:255",
            "email"         => "required|email|unique:users",
            "phone-number"  => "required|min:11|max:13|regex:/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im",
            "gender"        => "in:1,2",
            "birth-date"    => "date"
        ];
    }

    public function fromRequest(Request $request) {
        
        $this->first_name   = $request->get('first-name');
        $this->last_name    = $request->get('surname');
        $this->email        = $request->get('email');
        $this->phone_number = $request->get('phone-number');
        $this->gender       = $request->get('gender');
        $this->birth_date   = Carbon::parse($request->get('birth-date'))->format("Y-m-d");

        return $this;
    }
}
