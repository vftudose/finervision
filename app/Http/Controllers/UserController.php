<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index(Request $request)
    {
    	return view('user');
    }

    public function create(Request $request) 
    {
    	$user = new User();

    	$this->validate($request,$user->getValidationRules());

	    $saved = $user->fromRequest($request)->save();
    	
    	if(! $saved) {
    		return response(['status'=>'error',"message"=>"Something went wrong, please try again"],400);
    	}

    	return response(["status" => "success","message" => "User saved successfully!"],200);
    }	
}
